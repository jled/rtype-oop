import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class KBListener extends KeyAdapter {

	public static boolean isPressedUp, isPressedDown, isPressedLeft, isPressedRight, isPressedSpace, isPressedEsc;
	public static boolean locked;

	public static int keyUpDownArePressed() {

		return isPressedUp ? isPressedDown ? 11 : 10 : isPressedDown ? 01 : 00;
	}

	public static int keyLeftRightArePressed() {

		return isPressedLeft ? isPressedRight ? 11 : 10 : isPressedRight ? 01 : 00;
	}

	public static boolean keySpacePressed() {

		return isPressedSpace;
	}

	public static boolean keyEscPressed() {

		return isPressedEsc;
	}

	@Override
	public void keyReleased(KeyEvent e) {

		int key = e.getKeyCode();
		if ( key == KeyEvent.VK_UP ) {
			isPressedUp = false;
		}
		if ( key == KeyEvent.VK_DOWN ) {
			isPressedDown = false;
		}
		if ( key == KeyEvent.VK_LEFT ) {
			isPressedLeft = false;
		}
		if ( key == KeyEvent.VK_RIGHT ) {
			isPressedRight = false;
		}
		if ( key == KeyEvent.VK_SPACE ) {
			isPressedSpace = false;
		}
		if ( key == KeyEvent.VK_ESCAPE ) {
			isPressedEsc = false;
		}

	}

	@Override
	public void keyPressed(KeyEvent e) {

		int key = e.getKeyCode();
		if ( key == KeyEvent.VK_UP ) {
			isPressedUp = true;
		}
		if ( key == KeyEvent.VK_DOWN ) {
			isPressedDown = true;
		}
		if ( key == KeyEvent.VK_LEFT ) {
			isPressedLeft = true;
		}
		if ( key == KeyEvent.VK_RIGHT ) {
			isPressedRight = true;
		}
		if ( key == KeyEvent.VK_SPACE ) {
			isPressedSpace = true;
		}
		if ( key == KeyEvent.VK_ESCAPE ) {
			isPressedEsc = true;
		}

	}
}
